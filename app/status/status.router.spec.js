const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

const expect = chai.expect;
chai.use(sinonChai);

const sandbox = sinon.createSandbox();

const getSpy = sinon.spy();

const routerMock = {
  get: getSpy,
};

const express = require('express');
const {statusMessage} = require('./status.controller');

describe('status.router', () => {
  beforeEach(() => {
    sandbox.stub(express, 'Router').returns(routerMock);
    require('./status.router');
  });
  afterEach(() => {
    sandbox.restore();
  });
  it('calls router.get', () => {
    expect(getSpy).to.have.been.calledWith('/status', statusMessage);
  });
});
