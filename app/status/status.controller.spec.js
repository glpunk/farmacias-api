const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

const expect = chai.expect;
chai.use(sinonChai);

const controller = require('./status.controller');

describe('status.controller', () => {
  describe('.statusMessage', () => {
    it('retuns res.send result', () => {
      const res = {
        send: () => {
          return 'send called';
        },
      };
      result = controller.statusMessage({}, res);
      expect(result).to.equal('send called');
    });
    it('res.send is called with message', () => {
      const spy = sinon.spy();
      const res = {
        send: spy,
      };
      controller.statusMessage({}, res);
      expect(spy).to.have.been.calledWith({message: 'up and running'});
    });
  });
});
