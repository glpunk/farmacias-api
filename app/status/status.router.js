const express = require('express');
const router = express.Router();

const {statusMessage} = require('./status.controller');

router.get('/status', statusMessage);

module.exports = router;
