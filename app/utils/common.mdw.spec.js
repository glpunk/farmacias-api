const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

const expect = chai.expect;
chai.use(sinonChai);

const mdw = require('./common.mdw');

const sandbox = sinon.createSandbox();

describe('common.mdw', () => {
  beforeEach(() => {
  });
  afterEach(() => {
    sandbox.restore();
  });
  describe('.commonResponse', () => {
    it('calls res.send with locals result', async () => {
      const res = {
        send: (param) => {
          expect(param).to.equal('localResult');
        },
        locals: {
          result: 'localResult',
        },
      };
      mdw.commonResponse({}, res);
    });
  });
});
