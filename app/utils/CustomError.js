class CustomError extends Error {
  constructor(message, component, functionName, originalErr) {
    super(message);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, CustomError);
    }
    // Custom debugging information
    this.originalErr = originalErr;
    this.component = component;
    this.functionName = functionName;
  }
}

module.exports = CustomError;
