const bunyan = require('bunyan');
const path = require('path');
const fs = require('fs');
const rfs = require("rotating-file-stream");

const logDirectory = path.join(__dirname, '../../log');
 
// ensure log directory exists
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

// create a rotating write stream
const errorStream = rfs.createStream("error.log", {
  size: "100M",
  interval: "1d",
  path: logDirectory,
  compress: "gzip"
});

exports.loggerInstance = bunyan.createLogger({
    name: 'transaction-notifier',
    serializers: {
        req: require('bunyan-express-serializer'),
        res: bunyan.stdSerializers.res,
        err: bunyan.stdSerializers.err
    },
    level: 'info',
    streams: [{
      stream: errorStream
  }
    ]
});

exports.logError = function (id, error) {
    var log = this.loggerInstance.child({
        id: id,
        error
    }, true)
    log.error('error')
}