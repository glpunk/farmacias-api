const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const rewire = require('rewire');

const expect = chai.expect;
chai.use(sinonChai);

const CustomError = rewire('./CustomError');

const sandbox = sinon.createSandbox();

describe('CustomError', () => {
  beforeEach(() => {
  });
  afterEach(() => {
    sandbox.restore();
  });
  describe('.constructor', () => {
    it('sets custom values', () => {
      const origErr = Error('orgErr');
      const err = new CustomError('msg','comp','func', origErr);
      expect(err.component).to.equal('comp');
      expect(err.functionName).to.equal('func');
      expect(err.originalErr).to.equal(origErr);
    });
    it('calls Error.captureStackTrace', () => {
      const origErr = Error('orgErr');
      const spy = sandbox.spy(Error, 'captureStackTrace');
      const err = new CustomError('msg','comp','func', origErr);
      expect(spy).to.have.been.called;
    });
    it('not calls Error.captureStackTrace', () => {
      const origErr = Error('orgErr');
      Error.captureStackTrace = false;
      new CustomError('msg','comp','func', origErr);
    });
  });
});
