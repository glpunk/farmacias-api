const got = require('got');
const config = require('config');
const CustomError = require('./../utils/CustomError');

module.exports = {
  async getData(idRegion) {
    try {
      const configValues = config.get('endpoints.getLocalesRegion');
      return await got(
          'getLocalesRegion', {
            prefixUrl: configValues.prefixUrl,
            searchParams: {id_region: idRegion},
            timeout: configValues.timeout,
            retry: configValues.retry,
            responseType: 'json',
          },
      );
    } catch (error) {
      throw new CustomError(
          'error getting stores data', 'stores.service', 'getData', error,
      );
    }
  },
};
