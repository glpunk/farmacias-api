const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

const expect = chai.expect;
chai.use(sinonChai);

const sandbox = sinon.createSandbox();

const getSpy = sinon.spy();

const routerMock = {
  get: getSpy,
};

const express = require('express');
const {commonResponse} = require('../utils/common.mdw');
const {validateRequest, callController} = require('./stores.mdw');

describe('stores.router', () => {
  beforeEach(() => {
    sandbox.stub(express, 'Router').returns(routerMock);
    require('./stores.router');
  });
  afterEach(() => {
    sandbox.restore();
  });
  it('calls router.get', () => {
    expect(getSpy).to.have.been.calledWith(
        '/stores', validateRequest, callController, commonResponse,
    );
  });
});
