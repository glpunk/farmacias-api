const createError = require('http-errors');
const storesController = require('./stores.controller');
const {check, validationResult} = require('express-validator');

module.exports = {
  async validateRequest(req, res, next) {
    const validations = [
      check('id_region')
          .exists().matches(/^[1-9]|1[0-6]$/).withMessage('not in range'),
      check('sector')
          .optional().matches(/[a-z A-Z]/).withMessage('must contain a word'),
      check('name')
          .optional().matches(/[a-z A-Z]/).withMessage('must contain a word'),
    ];
    await Promise.all(validations.map((validation) => validation.run(req)));
    const errors = validationResult(req);
    if (errors.isEmpty()) {
      return next();
    }
    return next(
        createError(400, 'Bad Request', {errors: errors.array()}),
    );
  },
  async callController(req, res, next) {
    try {
      res.locals.result = await storesController.getStores(
          req.query.id_region,
          req.query.sector,
          req.query.name,
      );
      return next();
    } catch (error) {
      return next(createError(409, error));
    }
  },
};
