const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const rewire = require('rewire');
const service = rewire('./stores.service');

const expect = chai.expect;
chai.use(sinonChai);

const sandbox = sinon.createSandbox();
const gotStub = sandbox.stub();

describe('stores.service', () => {
  beforeEach(() => {
    service.__set__('got', gotStub);
  });
  afterEach(() => {
    gotStub.reset();
    sandbox.restore();
  });
  describe('.getData', () => {
    it('returns request response', async () => {
      gotStub.resolves([{store_id: 1}]);
      const res = await service.getData(7);
      expect(res).to.deep.equal([{store_id: 1}]);
    });
    it('throws an error', async () => {
      try {
        gotStub.rejects();
        await service.getData(7);
      } catch (error) {
        expect(error.message).to.equal('error getting stores data');
      }
    });
  });
});
