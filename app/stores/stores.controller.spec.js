const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const rewire = require('rewire');

const controller = rewire('./stores.controller');

const expect = chai.expect;
chai.use(sinonChai);

const sandbox = sinon.createSandbox();
const getDataStub = sandbox.stub();

const serviceMock = {
  getData: getDataStub,
};

const stores = [
  {
    'fecha': '29-12-2019',
    'local_id': '753',
    'local_nombre': 'AHUMADA',
    'comuna_nombre': 'BUIN',
    'localidad_nombre': 'BUIN',
    'local_direccion': 'SAN MARTIN 174',
    'funcionamiento_hora_apertura': '09:00 hrs.',
    'funcionamiento_hora_cierre': '22:00 hrs.',
    'local_telefono': '+560226313086',
    'local_lat': '-33.732',
    'local_lng': '-70.735941',
    'funcionamiento_dia': 'domingo',
    'fk_region': '7',
    'fk_comuna': '83',
  },
  {
    'fecha': '29-12-2019',
    'local_id': '773',
    'local_nombre': 'CRUZ VERDE',
    'comuna_nombre': 'CERRILLOS',
    'localidad_nombre': 'CERRILLOS',
    'local_direccion': 'AV. AMERICO VESPUCIO 1501',
    'funcionamiento_hora_apertura': '09:30 hrs.',
    'funcionamiento_hora_cierre': '21:30 hrs.',
    'local_telefono': '+560225396810',
    'local_lat': '-33.515827',
    'local_lng': '-70.71547',
    'funcionamiento_dia': 'domingo',
    'fk_region': '7',
    'fk_comuna': '85',
  },
  {
    'fecha': '29-12-2019',
    'local_id': '5915',
    'local_nombre': 'DEL DR. SIMI',
    'comuna_nombre': 'INDEPENDENCIA',
    'localidad_nombre': 'INDEPENDENCIA',
    'local_direccion': 'AVENIDA INDEPENDENCIA N° 565',
    'funcionamiento_hora_apertura': '09:00 hrs.',
    'funcionamiento_hora_cierre': '21:00 hrs.',
    'local_telefono': '+56',
    'local_lat': '-33.424567',
    'local_lng': '-70.654489',
    'funcionamiento_dia': 'domingo',
    'fk_region': '7',
    'fk_comuna': '94',
  },
  {
    'fecha': '29-12-2019',
    'local_id': '5921',
    'local_nombre': 'CRUZ VERDE',
    'comuna_nombre': 'LAS CONDES',
    'localidad_nombre': 'LAS CONDES',
    'local_direccion': 'AVENIDA PADRE HURTADO SUR 785',
    'funcionamiento_hora_apertura': '10:00 hrs.',
    'funcionamiento_hora_cierre': '21:30 hrs.',
    'local_telefono': '+56',
    'local_lat': '-33.415884',
    'local_lng': '-70.539431',
    'funcionamiento_dia': 'domingo',
    'fk_region': '7',
    'fk_comuna': '102',
  },
  {
    'fecha': '29-12-2019',
    'local_id': '5935',
    'local_nombre': 'SALCOBRAND',
    'comuna_nombre': 'MACUL',
    'localidad_nombre': 'MACUL',
    'local_direccion': 'AVENIDA MACUL N° 3019, LOCALES 2, 3 Y 4',
    'funcionamiento_hora_apertura': '09:00 hrs.',
    'funcionamiento_hora_cierre': '21:00 hrs.',
    'local_telefono': '+56',
    'local_lat': '-33.482241',
    'local_lng': '-70.599307',
    'funcionamiento_dia': 'domingo',
    'fk_region': '7',
    'fk_comuna': '106',
  },
];

describe('stores.controller', () => {
  beforeEach(() => {
    controller.__set__('storesService', serviceMock);
  });
  afterEach(() => {
    getDataStub.reset();
    sandbox.restore();
  });
  describe('.getStores', () => {
    it('calls storesService.getData with idRegion', async () => {
      getDataStub.resolves({body: []});
      await controller.getStores(7);
      expect(getDataStub).to.have.been.calledWith(7);
    });
    it('returns body response', async () => {
      getDataStub.resolves({body: []});
      const result = await controller.getStores(7);
      expect(result).to.deep.equal([]);
    });
    it('throws catchd error', async () => {
      try {
        getDataStub.throws(Error('service error'));
        await controller.getStores(7);
      } catch (error) {
        expect(error.message).to.equal('service error');
      }
    });
  });
  describe('.filterResult', () => {
    it('calls filterByProperty with comuna_nombre', () => {
      const spy = sandbox.spy(controller, 'filterByProperty');
      controller.filterResult([], 'sector', 'nombre');
      expect(spy).to.have.been.calledWith([], 'comuna_nombre', 'sector');
    });
    it('calls filterByProperty with local_nombre', () => {
      const spy = sandbox.spy(controller, 'filterByProperty');
      controller.filterResult([], 'sector', 'nombre');
      expect(spy).to.have.been.calledWith([], 'local_nombre', 'nombre');
    });
  });
  describe('.filterByProperty', () => {
    it('returns if not value', () => {
      const res = controller.filterByProperty(
          stores, 'comuna_nombre', undefined,
      );
      expect(res.length).to.equal(5);
    });
    it('filter body by comuna_nombre', () => {
      const res = controller.filterByProperty(
          stores, 'comuna_nombre', 'INDEPENDE',
      );
      expect(res.length).to.equal(1);
    });
    it('filter body by local_nombre', () => {
      const res = controller.filterByProperty(
          stores, 'local_nombre', 'cruz verde',
      );
      expect(res.length).to.equal(2);
    });
  });
  describe('.mapResult', () => {
    const res = controller.mapResult([stores[0]]);
    expect(res).to.deep.equal([
      {
        name: 'AHUMADA',
        address: 'SAN MARTIN 174',
        phone: '+560226313086',
        lat: '-33.732',
        lng: '-70.735941',
      },
    ]);
  });
});
