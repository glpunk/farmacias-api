const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const rewire = require('rewire');

const expect = chai.expect;
chai.use(sinonChai);

const mdw = rewire('./stores.mdw');

const sandbox = sinon.createSandbox();
const getStoresStub = sandbox.stub();
const nextSpy = sandbox.spy();
const errorStub = sandbox.stub();

const storesControllerMock = {
  getStores: getStoresStub,
};

const req = {
  query: {
    id_region: 7,
  },
};
const res = {
  locals: {
    result: null,
  },
};

describe('stores.mdw', () => {
  beforeEach(() => {
    mdw.__set__('createError', errorStub);
    mdw.__set__('storesController', storesControllerMock);
  });
  afterEach(() => {
    errorStub.reset();
    getStoresStub.reset();
    sandbox.restore();
  });
  describe('.validateRequest', () => {
    context('good request', () => {
      it('id_region exists', async () => {
        await mdw.validateRequest(req, res, nextSpy);
        expect(nextSpy).to.have.been.called;
      });
      it('id_region is a number', async () => {
        await mdw.validateRequest(req, res, nextSpy);
        expect(nextSpy).to.have.been.called;
      });
    });
    context('bad request', () => {
      it('throws id_region not existance', async () => {
        await mdw.validateRequest({}, res, nextSpy);
        expect(nextSpy).to.have.been.called;
        expect(errorStub).to.have.been.calledWith(400);
      });
      it('throws id_region is not a number', async () => {
        const req1 = {
          query: {
            id_region: 'abc',
          },
        };
        await mdw.validateRequest(req1, res, nextSpy);
        expect(nextSpy).to.have.been.called;
        expect(errorStub).to.have.been.calledWith(400);
      });
      it('throws sector is not a string', async () => {
        const req1 = {
          query: {
            id_region: 7,
            sector: 11,
          },
        };
        await mdw.validateRequest(req1, res, nextSpy);
        expect(nextSpy).to.have.been.called;
        expect(errorStub).to.have.been.calledWith(400);
      });
      it('throws name is not a string', async () => {
        const req1 = {
          query: {
            id_region: 7,
            name: 11,
          },
        };
        await mdw.validateRequest(req1, res, nextSpy);
        expect(nextSpy).to.have.been.called;
        expect(errorStub).to.have.been.calledWith(400);
      });
    });
  });
  describe('.callController', () => {
    it('sets res.locals.result', async () => {
      getStoresStub.resolves('controllerResult');
      await mdw.callController(req, res, nextSpy);
      expect(res.locals.result).to.equal('controllerResult');
    });
    it('returns next()', async () => {
      getStoresStub.resolves('controllerResult');
      await mdw.callController(req, res, nextSpy);
      expect(nextSpy).to.have.been.called;
    });
    it('catches an error', async () => {
      getStoresStub.rejects(new Error('controller error'));
      await mdw.callController(req, res, nextSpy);
      expect(nextSpy).to.have.been.called;
      expect(errorStub).to.have.been.calledWith(409);
    });
  });
});
