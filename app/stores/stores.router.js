const express = require('express');
const router = express.Router();

const {commonResponse} = require('../utils/common.mdw');
const {validateRequest, callController} = require('./stores.mdw');

router.get('/stores', validateRequest, callController, commonResponse);

module.exports = router;
