const storesService = require('./stores.service');

module.exports = {
  async getStores(idRegion, sector, name) {
    try {
      const serviceResponse = await storesService.getData(idRegion);
      body = this.filterResult(serviceResponse.body, sector, name);
      body = this.mapResult(body);
      return body;
    } catch (error) {
      throw error;
    }
  },
  filterResult(body, sector, name) {
    body = this.filterByProperty(body, 'comuna_nombre', sector);
    body = this.filterByProperty(body, 'local_nombre', name);
    return body;
  },
  filterByProperty(body, property, value) {
    if (!value) return body;
    const exp = new RegExp(value, 'i');
    return body.filter((store) => exp.test(store[property]));
  },
  mapResult(body) {
    return body.map((obj) => {
      const rObj = {};
      rObj['name'] = obj.local_nombre;
      rObj['address'] = obj.local_direccion;
      rObj['phone'] = obj.local_telefono;
      rObj['lat'] = obj.local_lat;
      rObj['lng'] = obj.local_lng;
      return rObj;
    });
  },
};
