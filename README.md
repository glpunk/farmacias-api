# farmacias-api
Ejercicio de api de farmacias con framework express

### Requisitos
NodeJS versión 12

npm para instalación de dependencias

Docker para ejecución dentro de stack de servicios

### Instalación
```
npm install
```
### Ejecución en desarollo
```
npm run start
```
Prueba por explorador:

http://127.0.0.1:3000/stores?id_region=7

### Pruebas unitarias
```
npm run test
```
### Cobertura de pruebas
```
npm run coverage
```
### Docker
Construcción de imagen

```
docker build . -t farmacias-api 
```

Para ejecutar como stack de servcios ver repositorio [farmacias-stack](https://gitlab.com/glpunk/farmacias-stack)