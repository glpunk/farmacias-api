FROM keymetrics/pm2:latest-alpine

RUN apk add --no-cache --virtual .gyp \
        python \
        make \
        g++

# Bundle APP files
COPY bin bin/
COPY app app/
COPY config config/
COPY app.js .
COPY routes.js .
COPY package.json .
COPY pm2.json .

# Install app dependencies
ENV NPM_CONFIG_LOGLEVEL warn
RUN npm install --production

RUN apk del .gyp

EXPOSE 3000

CMD [ "pm2-runtime", "start", "pm2.json" ]