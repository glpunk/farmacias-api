const createError = require('http-errors');
const express = require('express');
const morgan = require('morgan');
const logger = require('./app/utils/logger');
const routes = require('./routes');
const addRequestId = require('express-request-id')();
const path = require('path');
const fs = require('fs');
const rfs = require('rotating-file-stream');
var cors = require('cors')

const app = express();

app.use(addRequestId);

var whitelist = [
  'http://127.0.0.1',
  'http://127.0.0.1:5000',
  'http://172.17.0.1',
  'http://172.17.0.1:5000'
];
var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({extended: false}));

const logDirectory = path.join(__dirname, 'log');

// ensure log directory exists
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

morgan.token('id', function getId(req) {
  return req.id;
});

// create a rotating write stream
const accessStream = rfs.createStream('access.log', {
  size: '10M',
  interval: '1d',
  path: logDirectory,
  compress: 'gzip',
});

const loggerFormat = ':id [:date[web]] ":method :url" :status :response-time';
app.use(morgan(loggerFormat, {stream: accessStream}));

// Hook up all routes
app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // send the error page
  logger.logError(req.id, err);
  res.status(err.status || 500);
  res.send({error: err.message});
});

module.exports = app;
