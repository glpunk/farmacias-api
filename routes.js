const express = require('express');
const router = express.Router();

const statusRouter = require('./app/status/status.router');
const storesRouter = require('./app/stores/stores.router');

router.use('/', statusRouter);
router.use('/', storesRouter);

module.exports = router;